:- module(ucs,
    [ ucs/3
    , ucs/4
    , ucs/5
    ]
).

/** <module> Uniform Cost Search

Provides generic uniform cost search algorithm: searches tree by selecting the lowest
cost branch first.

---

**Note**: predicate to GetChildren must return Cost-Node pairs

---

Example:

```example
:- use_module(library(search/ucs)).

/* Directed Graph

┌─┐2  ┌─┐1  ┌─┐
│a├──▶│b├──▶│c│
└─┘   └┬┘   └┬┘
       │5    │1
       ▼     ▼
      ┌───────┐2  ┌─┐
      │   d   ├──▶│e│
      └───────┘   └─┘

*/

edge(a, b, 2).
edge(b, c, 1).
edge(b, d, 5).
edge(c, d, 1).
edge(d, e, 2).

get_children(Node, Children) :-
    setof(Cost-Child, edge(Node, Child, Cost), Children).
```

Query:
```
?- ucs(get_children, a, e).
true.

?- ucs(get_children, a, X).
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
X = d ;
X = e ;
false.
```

With Costs:

```
?- ucs(get_children, a, X).
X = a,
C = 0 ;
X = b
C = 2 ;
X = c
C = 3 ;
X = d
C = 4 ;
X = e
C = 6 ;
X = d
C = 7 ;
X = e
C = 9 ;
false.
```

Extending the example to work with a goal predicate:

```
my_goal(N) :-
    N = e.

?- ucs(get_children, my_goal, a, X, [path(Path), cost(Cost)]).
X = e,
Path = [a, b, c, d, e]
Cost = 6 ;
X = e,
Path = [a, b, d, e]
Cost = 9 ;
false.

```

@author Paul Brown

*/

:- use_module(lib/queue).
:- use_module(lib/common,
    [ goal_check/5
    , all_cost_children/5
    , path_check/2
    ]
).

:- meta_predicate ucs(2, ?, ?).
%! ucs(++GetChildren, ?Origin, ?Destination) is nondet.
%
% Uniform Cost Search from Origin
% to Destination. Equivalent to
% ucs(Origin, Destination, []).
ucs(GC, Origin, Destination) :-
    singleton_heap(heap(Heap, Size), 0, node(Origin, [])),
    ucs_star(Heap, Size, GC, =(Destination), Destination, _, _).

:- meta_predicate ucs(2, ?, ?, +).
%! ucs(++GetChildren, ?Origin, ?Destination, Options) is nondet.
%
% Uniform Cost Search from Origin to
% Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
ucs(GC, Origin, Destination, Options) :-
    ucs(GC, =(Destination), Origin, Destination, Options).

:- meta_predicate ucs(2, 1, ?, ?, +).
%! ucs(++GetChildren, +Goal, ?Origin, ?Destination, Options) is nondet.
%
% Uniform Cost Search from Origin to
% Destination calling Goal on a node to determine Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
% @arg Goal A predicate with arity one that is true if Destination is a valid destination
ucs(GC, Goal, Origin, Destination, Options) :-
    singleton_heap(heap(Heap, Size), 0, node(Origin, [])),
    ucs_star(Heap, Size, GC, Goal, Destination, Path, Cost),
    path_check(Path, Options),
    cost_check(Cost, Options).


%! plain_ucs_star(Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a ucs by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
ucs_star(Heap, Size, GC, Goal, Destination, Path, Cost) :-
    get_from_heap(heap(Heap, Size), C, node(X, Travelled), OpenPoppedX),
    ( goal_check(X, Goal, Travelled, Path, Destination), C = Cost
    ; all_cost_children(GC, X, C, Travelled, Children),
      merge_heaps(OpenPoppedX, Children, heap(Open, OpenSize)),
      ucs_star(Open, OpenSize, GC, Goal, Destination, Path, Cost)
    ).

cost_check(Cost, Options) :-
    member(cost(Cost), Options).
cost_check(_, Options) :-
    \+ memberchk(cost(_), Options).
