:- module(iterative_deepening,
    [ iterative_deepening/3
    , iterative_deepening/4
    , iterative_deepening/5
    ]
).

/** <module> Iterative Deepening Search

Provides depth first search algorithm to an increasing limited depth.

---

** Note **: Iterative Deepening increases the search depth to infinity, or you run out of memory, these queries will all continue to generate often repeat
solutions.

---


Example:

```example
:- use_module(library(search/dls)).

/* Directed Graph

┌─┐   ┌─┐   ┌─┐
│a├──▶│b├──▶│c│
└─┘   └┬┘   └┬┘
       │     │
       ▼     ▼
      ┌───────┐   ┌─┐
      │   d   ├──▶│e│
      └───────┘   └─┘

*/

edge(a, b).
edge(b, c).
edge(b, d).
edge(c, d).
edge(d, e).

get_children(Node, Children) :-
    setof(Child, edge(Node, Child), Children).
```

Query:
```
?- iterative_deepeing(get_children, a, e).
true ;
true ;
true ;
...
```

?- iterative_deepening(get_children, a, X).
X = a ;
X = a ;
X = b ;
X = a ;
X = b ;
X = c ;
X = d ;
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
...

?- iterative_deepening(get_children, a, X, [path(Path)]).
X = a,
Path = [a] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, d] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, d] ;
X = e,
Path = [a, b, d, e] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, d] ;
X = e,
Path = [a, b, d, e] ;
X = a,
Path = [a]
...
```

Query without extended_list:
```
?- iterative_deepening(get_children, a, X, [path(Path), extended_list(false)]).
X = a,
Path = [a] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, d] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, c, d] ;
X = d,
Path = [a, b, d] ;
X = e,
Path = [a, b, d, e] ;
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, c, d] ;
X = e,
Path = [a, b, c, d, e] ;
X = d,
Path = [a, b, d] ;
X = e,
Path = [a, b, d, e] ;
X = a,
Path = [a]
...
```


Extending the example to work with a goal predicate:

```
my_goal(N) :-
    N = e.

?- iterative_deepening(get_children, my_goal, a, X, [path(Path)]).
X = e,
Path = [a, b, d, e] ;
X = e,
Path = [a, b, d, e] ;
X = e,
Path = [a, b, d, e] ;
...

?- iterative_deepening(get_children, my_goal, a, X, [path(Path), extended_list(false)]).
X = e,
Path = [a, b, d, e] ;
X = e,
Path = [a, b, c, d, e] ;
X = e,
Path = [a, b, d, e] ;
X = e,
Path = [a, b, c, d, e] ;
...

```

@author Paul Brown

*/

:- use_module(lib/queue).
:- use_module(lib/common,
    [ goal_check/5
    , all_children/6
    , extended_list_children/8
    , path_check/2
    ]
).
:- use_module(dls).

:- meta_predicate iterative_deepening(2, ?, ?).
%! iterative_deepening(?GetChildren, Origin, Destination)
% Iterative Deepening Search from Origin to Destination.
%
% Equivalent to iterative_deepening(GetChildren, Origin, Destination, []).
iterative_deepening(GetChildren, Origin, Destination) :-
    iterative_deepening(GetChildren, Origin, Destination, []).

:- meta_predicate iterative_deepening(2, ?, ?, +).
%! dls(++GetChildren, ?Origin, ?Destination, Options) is nondet.
%
% Iterative Deepening Search from Origin to Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
iterative_deepening(GC, Origin, Destination, Options) :-
    it_dep(1, GC, Origin, Destination, Options).

it_dep(N, GC, Origin, Destination, Options) :-
    dls(GC, =(Destination), Origin, Destination, [limit(N)|Options]).
it_dep(N, GC, Origin, Destination, Options) :-
    succ(N, M),
    it_dep(M, GC, Origin, Destination, Options).

:- meta_predicate iterative_deepening(2, 1, ?, ?, +).
%! dls(++GetChildren, +Goal, ?Origin, ?Destination, Options) is nondet.
%
% Iterative Deepening Search from Origin to
% Destination calling Goal on a node to determine Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
% @arg Goal A predicate with arity one that is true if Destination is a valid destination
iterative_deepening(GC, Goal, Origin, Destination, Options) :-
    it_dep(1, GC, Goal, Origin, Destination, Options).

it_dep(N, GC, Goal, Origin, Destination, Options) :-
    dls(GC, Goal, Origin, Destination, [limit(N)|Options]).
it_dep(N, GC, Goal, Origin, Destination, Options) :-
    succ(N, M),
    it_dep(M, GC, Goal, Origin, Destination, Options).
