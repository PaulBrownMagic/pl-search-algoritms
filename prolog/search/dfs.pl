:- module(dfs,
    [ dfs/3
    , dfs/4
    , dfs/5
    ]
).

/** <module> Depth First Search

Provides generic depth first search algorithms.

---

**Note**: without extended list, DFS can get stuck in loops, never terminating.

---

---

**Note**: DFS will fail to terminate when searching an infinite graph if the
solution exists in any branch other than the first taken.

---

Example:

```example
:- use_module(library(search/dfs)).

/* Directed Graph

┌─┐   ┌─┐   ┌─┐
│a├──▶│b├──▶│c│
└─┘   └┬┘   └┬┘
       │     │
       ▼     ▼
      ┌───────┐   ┌─┐
      │   d   ├──▶│e│
      └───────┘   └─┘

*/

edge(a, b).
edge(b, c).
edge(b, d).
edge(c, d).
edge(d, e).

get_children(Node, Children) :-
    setof(Child, edge(Node, Child), Children).
```

Query:
```
?- dfs(get_children, a, e).
true.
```

Query without extended_list:
```
?- dfs(get_children, a, X, [extended_list(false)]).
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
X = d ;
X = e ;
false.
```

Query with extended_list (default):

```
?- dfs(get_children, a, X).
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
false.
```

Extending the example to work with a goal predicate:

```
my_goal(N) :-
    N = e.

?- dfs(get_children, my_goal, a, X, [path(Path)]).
X = e,
Path = [a, b, d, e] ;
false.

?- dfs(get_children, my_goal, a, X, [path(Path), extended_list(false)]).
X = e,
Path = [a, b, c, d, e] ;
X = e,
Path = [a, b, d, e] ;
false.

```

@author Paul Brown

*/

:- use_module(lib/queue).
:- use_module(lib/common,
    [ goal_check/5
    , all_children/4
    , extended_list_children/6
    , path_check/2
    ]
).

:- meta_predicate dfs(2, ?, ?).
%! dfs(++GetChildren, ?Origin, ?Destination) is nondet.
%
% Depth-First Search from Origin
% to Destination. Equivalent to
% dfs(Origin, Destination, []).
dfs(GC, Origin, Destination) :-
    queue(node(Origin, []), q(N, F, B)),
    extended_list_dfs_star(N, F, B, GC, =(Destination), [Origin], Destination, _).

:- meta_predicate dfs(2, ?, ?, +).
%! dfs(++GetChildren, ?Origin, ?Destination, Options) is nondet.
%
% Depth-First Search from Origin to
% Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
dfs(GC, Origin, Destination, Options) :-
    dfs(GC, =(Destination), Origin, Destination, Options).

:- meta_predicate dfs(2, 1, ?, ?, +).
%! dfs(++GetChildren, +Goal, ?Origin, ?Destination, Options) is nondet.
%
% Depth-First Search from Origin to
% Destination calling Goal on a node to determine Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
% @arg Goal A predicate with arity one that is true if Destination is a valid destination
dfs(GC, Goal, Origin, Destination, Options) :-
    queue(node(Origin,[]), q(N, F, B)),
    dfs_star(N, F, B, GC, Goal, Origin, Destination, Path, Options),
    path_check(Path, Options).


%! plain_dfs_star(Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a dfs by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
plain_dfs_star(N, F, B, GC, Goal, Destination, Path) :-
    queue_head(node(X, Travelled), OpenPoppedX, q(N, F, B)), % pop the current node
    ( goal_check(X, Goal, Travelled, Path, Destination)
    ; all_children(GC, X, Travelled, Children),
      queue_head_list(Children, OpenPoppedX, q(Nn, Fn, Bn)), % Put children on head of queue for depth-first search
      plain_dfs_star(Nn, Fn, Bn, GC, Goal, Destination, Path)
    ).

%! extended_list_dfs_star(Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a dfs by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
%
%  Omits visited nodes by maintaining a list of those that have been extended
extended_list_dfs_star(N, F, B, GC, Goal, Closed, Destination, Path) :-
    queue_head(node(X, Travelled), OpenPoppedX, q(N, F, B)), % pop the current node
    ( goal_check(X, Goal, Travelled, Path, Destination)
    ; extended_list_children(GC, X, Travelled, Closed, Closed1, Children),
      queue_head_list(Children, OpenPoppedX, q(Nn, Fn, Bn)), % Put children on head of queue for depth-first search
      extended_list_dfs_star(Nn, Fn, Bn, GC, Goal, Closed1, Destination, Path) % recursive call to continue search
    ).

%! dfs_star(+N, +F, +B, ++GetChildren, ++Goal, ?Origin, ?Destination, ?Path, +Options)
% determine whether to do dfs with or without extended list and execute it
dfs_star(N, F, B, GC, Goal, Origin, Destination, Path, Options) :-
    \+ memberchk(extended_list(false), Options),
    extended_list_dfs_star(N, F, B, GC, Goal, [Origin], Destination, Path).
dfs_star(N, F, B, GC, Goal, _Origin, Destination, Path, Options) :-
    memberchk(extended_list(false), Options),
    plain_dfs_star(N, F, B, GC, Goal, Destination, Path).
