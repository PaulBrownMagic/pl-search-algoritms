:- module(dls,
    [ dls/4
    , dls/5
    ]
).

/** <module> Depth Limited Search

Provides generic depth first search algorithms to a limited depth.


Example:

```example
:- use_module(library(search/dls)).

/* Directed Graph

┌─┐   ┌─┐   ┌─┐
│a├──▶│b├──▶│c│
└─┘   └┬┘   └┬┘
       │     │
       ▼     ▼
      ┌───────┐   ┌─┐
      │   d   ├──▶│e│
      └───────┘   └─┘

*/

edge(a, b).
edge(b, c).
edge(b, d).
edge(c, d).
edge(d, e).

get_children(Node, Children) :-
    setof(Child, edge(Node, Child), Children).
```

Query:
```
?- dls(get_children, a, e, [limit(4)]).
true.

?- dls(get_children, a, e, [limit(3)]).
false.
```

Effect of decreasing depth limit:

```
?- dls(dls:get_children, a, X, [limit(5), path(Path), extended_list(false)]).
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, c, d] ;
X = e,
Path = [a, b, c, d, e] ;
X = d,
Path = [a, b, d] ;
X = e,
Path = [a, b, d, e] ;
false.

?- dls(dls:get_children, a, X, [limit(4), path(Path), extended_list(false)]).
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, c, d] ;
X = d,
Path = [a, b, d] ;
X = e,
Path = [a, b, d, e] ;
false.

?- dls(dls:get_children, a, X, [limit(3), path(Path), extended_list(false)]).
X = a,
Path = [a] ;
X = b,
Path = [a, b] ;
X = c,
Path = [a, b, c] ;
X = d,
Path = [a, b, d] ;
false.

```

Query without extended_list:
```
?- dls(get_children, a, X, [limit(5), extended_list(false)]).
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
X = d ;
X = e ;
false.

?- dls(get_children, a, X, [limit(4), extended_list(false)]).
X = a ;
X = b ;
X = c ;
X = d ;
X = d ;
X = e ;
```

Query with extended_list (default):

```
?- dls(get_children, a, X, [limit(5)]).
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
false.
```

Extending the example to work with a goal predicate:

```
my_goal(N) :-
    N = e.

?- dls(get_children, my_goal, a, X, [limit(5), path(Path)]).
X = e,
Path = [a, b, d, e] ;
false.

?- dls(get_children, my_goal, a, X, [limit(5), path(Path), extended_list(false)]).
X = e,
Path = [a, b, c, d, e] ;
X = e,
Path = [a, b, d, e] ;
false.

?- dls(get_children, my_goal, a, X, [limit(4), path(Path), extended_list(false)]).
X = e,
Path = [a, b, d, e] ;
false.
```

@author Paul Brown

*/

:- use_module(lib/queue).
:- use_module(lib/common,
    [ goal_check/5
    , all_children/6
    , extended_list_children/8
    , path_check/2
    ]
).

:- meta_predicate dls(2, ?, ?, +).
%! dls(++GetChildren, ?Origin, ?Destination, Options) is nondet.
%
% Depth-First Search from Origin to
% Destination.
%
% Options:
% - `limit(N)` where N is the depth-limited for the search. N must be an integer.
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
dls(GC, Origin, Destination, Options) :-
    dls(GC, =(Destination), Origin, Destination, Options).

:- meta_predicate dls(2, 1, ?, ?, +).
%! dls(++GetChildren, +Goal, ?Origin, ?Destination, Options) is nondet.
%
% Depth-First Search from Origin to
% Destination calling Goal on a node to determine Destination.
%
% Options:
% - `limit(N)` where N is the depth-limited for the search. N must be an integer.
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
% @arg Goal A predicate with arity one that is true if Destination is a valid destination
dls(GC, Goal, Origin, Destination, Options) :-
    member(limit(DL), Options), integer(DL), DL > 0, succ(AL, DL),
    queue(node(Origin, [])-0, q(N, F, B)),
    dls_star(AL, N, F, B, GC, Goal, Origin, Destination, Path, Options),
    path_check(Path, Options).


%! plain_dls_star(DepthLimit, Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a dls by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
plain_dls_star(DL, N, F, B, GC, Goal, Destination, Path) :-
    queue_head(node(X, Travelled)-CD, OpenPoppedX, q(N, F, B)), % pop the current node
    ( goal_check(X, Goal, Travelled, Path, Destination)
    ; all_children(CD, DL, GC, X, Travelled, Children),
      queue_head_list(Children, OpenPoppedX, q(Nn, Fn, Bn)), % Put children on head of queue for depth-first search
      plain_dls_star(DL, Nn, Fn, Bn, GC, Goal, Destination, Path)
    ).

%! extended_list_dls_star(DepthLimit, Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a dls by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
%
%  Omits visited nodes by maintaining a list of those that have been extended
extended_list_dls_star(DL, N, F, B, GC, Goal, Closed, Destination, Path) :-
    queue_head(node(X, Travelled)-CD, OpenPoppedX, q(N, F, B)), % pop the current node
    ( goal_check(X, Goal, Travelled, Path, Destination)
    ; extended_list_children(CD, DL, GC, X, Travelled, Closed, Closed1, Children),
      queue_head_list(Children, OpenPoppedX, q(Nn, Fn, Bn)), % Put children on head of queue for depth-first search
      extended_list_dls_star(DL, Nn, Fn, Bn, GC, Goal, Closed1, Destination, Path) % recursive call to continue search
    ).

%! dls_star(+N, +F, +B, ++GetChildren, ++Goal, ?Origin, ?Destination, ?Path, +Options)
% determine whether to do dls with or without extended list and execute it
dls_star(DL, N, F, B, GC, Goal, Origin, Destination, Path, Options) :-
    \+ memberchk(extended_list(false), Options),
    extended_list_dls_star(DL, N, F, B, GC, Goal, [Origin], Destination, Path).
dls_star(DL, N, F, B, GC, Goal, _Origin, Destination, Path, Options) :-
    memberchk(extended_list(false), Options),
    plain_dls_star(DL, N, F, B, GC, Goal, Destination, Path).
