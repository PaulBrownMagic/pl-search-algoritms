:- module(bfs,
    [ bfs/3
    , bfs/4
    , bfs/5
    ]
).

/** <module> Breadth First Search

Provides generic breadth first search algorithms.


Example:

```example
:- use_module(library(search/bfs)).

/* Directed Graph

┌─┐   ┌─┐   ┌─┐
│a├──▶│b├──▶│c│
└─┘   └┬┘   └┬┘
       │     │
       ▼     ▼
      ┌───────┐   ┌─┐
      │   d   ├──▶│e│
      └───────┘   └─┘

*/

edge(a, b).
edge(b, c).
edge(b, d).
edge(c, d).
edge(d, e).

get_children(Node, Children) :-
    setof(Child, edge(Node, Child), Children).
```

Query:
```
?- bfs(get_children, a, e).
true.
```

Query without extended_list:
```
?- bfs(get_children, a, X, [extended_list(false)]).
X = a ;
X = b ;
X = c ;
X = d ;
X = d ;
X = e ;
X = e ;
false.
```

Query with extended_list (default):

```
?- bfs(get_children, a, X).
X = a ;
X = b ;
X = c ;
X = d ;
X = e ;
false.
```

Extending the example to work with a goal predicate:

```
my_goal(N) :-
    N = e.

?- bfs(get_children, my_goal, a, X, [path(Path)]).
X = e,
Path = [a, b, d, e] ;
false.

?- bfs(get_children, my_goal, a, X, [path(Path), extended_list(false)]).
X = e,
Path = [a, b, d, e] ;
X = e,
Path = [a, b, c, d, e] ;
false.

```

@author Paul Brown

*/

:- use_module(lib/queue).
:- use_module(lib/common,
    [ goal_check/5
    , all_children/4
    , extended_list_children/6
    , path_check/2
    ]
).

:- meta_predicate bfs(2, ?, ?).
%! bfs(++GetChildren, ?Origin, ?Destination) is nondet.
%
% Breadth-First Search from Origin
% to Destination. Equivalent to
% bfs(Origin, Destination, []).
bfs(GC, Origin, Destination) :-
    queue(node(Origin, []), q(N, F, B)),
    extended_list_bfs_star(N, F, B, GC, =(Destination), [Origin], Destination, _).

:- meta_predicate bfs(2, ?, ?, +).
%! bfs(?++GetChildren, Origin, ?Destination, Options) is nondet.
%
% Breadth-First Search from Origin to
% Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
bfs(GC, Origin, Destination, Options) :-
    bfs(GC, =(Destination), Origin, Destination, Options).

:- meta_predicate bfs(2, 1, ?, ?, +).
%! bfs(++GetChildren, +Goal, ?Origin, ?Destination, Options) is nondet.
%
% Breadth-First Search from Origin to
% Destination calling Goal on a node to determine Destination.
%
% Options:
% - `extended_list(Bool)` Whether to use the extended list to exclude visited nodes. Default is `extended_list(true)`. If you can guarantee a finite tree structure is being searched, setting this to false can improve efficiency.
% - `path(Path)` Returns the path in terms of nodes traversed from Origin to Destination
%
% @arg Origin The starting node/situation/state from which to begin the search
% @arg Destination A reachable, possibly goal node/situation/state from the Origin
% @arg Goal A predicate with arity one that is true if Destination is a valid destination
bfs(GC, Goal, Origin, Destination, Options) :-
    queue(node(Origin, []), q(N, F, B)),
    bfs_star(N, F, B, GC, Goal, Origin, Destination, Path, Options),
    path_check(Path, Options).


%! plain_bfs_star(Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a bfs by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
plain_bfs_star(N, F, B, GC, Goal, Destination, Path) :-
    queue_head(node(X, Travelled), OpenPoppedX, q(N, F, B)), % pop the current node
    ( goal_check(X, Goal, Travelled, Path, Destination)
    ; all_children(GC, X, Travelled, Children),
      queue_last_list(Children, OpenPoppedX, q(Nn, Fn, Bn)), % Put children on head of queue for depth-first search
      plain_bfs_star(Nn, Fn, Bn, GC, Goal, Destination, Path)
    ).

%! extended_list_bfs_star(Num, Front, Back, GetChildren, Goal, Destination, Path) is nondet.
%  Conduct a bfs by taking the next node from the queue(Num, Front, Back),
%  see if that node is the Destination, and recurse.
%
%  Omits visited nodes by maintaining a list of those that have been extended
extended_list_bfs_star(N, F, B, GC, Goal, Closed, Destination, Path) :-
    queue_head(node(X, Travelled), OpenPoppedX, q(N, F, B)), % pop the current node
    ( goal_check(X, Goal, Travelled, Path, Destination)
    ; extended_list_children(GC, X, Travelled, Closed, Closed1, Children),
      queue_last_list(Children, OpenPoppedX, q(Nn, Fn, Bn)), % Put children on head of queue for depth-first search
      extended_list_bfs_star(Nn, Fn, Bn, GC, Goal, Closed1, Destination, Path) % recursive call to continue search
    ).

%! bfs_star(+N, +F, +B, ++GetChildren, ++Goal, ?Origin, ?Destination, ?Path, +Options)
% determine whether to do bfs with or without extended list and execute it
bfs_star(N, F, B, GC, Goal, Origin, Destination, Path, Options) :-
    \+ memberchk(extended_list(false), Options),
    extended_list_bfs_star(N, F, B, GC, Goal, [Origin], Destination, Path).
bfs_star(N, F, B, GC, Goal, _Origin, Destination, Path, Options) :-
    memberchk(extended_list(false), Options),
    plain_bfs_star(N, F, B, GC, Goal, Destination, Path).
