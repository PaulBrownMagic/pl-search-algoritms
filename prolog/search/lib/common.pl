:- module(common,
    [ goal_check/5
    , all_children/4
    , all_children/6
    , all_cost_children/5
    , extended_list_children/6
    , extended_list_children/8
    , path_check/2
    ]
).

/** <module> Common Predicates

This module contains predicates common to
search algorithms such as getting the children nodes
and checking if a goal condition has been reached.

This module is not intended for public consumption.

*/

:- dynamic search:get_children/2.

%! goal_check(+Node, +Goal, +Travelled, -Path, -Destination) is semidet.
%  True if Node satisfies the Goal, Path is the route taken from origin
% to destination and Destination is the Node.
goal_check(Node, Goal, Travelled, Path, Node) :-
    call(Goal, Node),
    reverse([Node|Travelled], Path).

%! all_children(+Node, +Travelled:list, -Children: pairs) is det.
%! all_children(+CurrentDepth, +DepthLimit, +Node, +Travelled:list, -Children: pairs) is det.
% find all the children of the node and add the path
% travelled so far.
all_children(GC, Node, Travelled, Children) :-
    get_children(GC, Node, ChildNodes),
    zip_path([Node|Travelled], ChildNodes, Children).
% Return no children after the depth limit
all_children(DL, DL, _, _, _, []).
all_children(CD, DL, GC, Node, Travelled, Children) :-
    CD < DL, % Case of CD > DL not handled, will fail rather than return []
    all_children(GC, Node, Travelled, Cs),
    succ(CD, ND),
    zip_depth(ND, Cs, Children).

% Get all children as a heap with updated costs
all_cost_children(GC, Node, Cost, Travelled, ChildrenHeap) :-
    get_children(GC, Node, ChildNodes),
    maplist(add_cost(Cost), ChildNodes, Costed),
    zip_path_values([Node|Travelled], Costed, Children),
    list_to_heap(Children, ChildrenHeap).

%! extended_list_children(+Node, +Travelled: list, +Closed:set, -NewClosed:set, -Children: pairs) is det.
%! extended_list_children(+CurrentDepth, +DepthLimit, +Node, +Travelled: list, +Closed:set, -NewClosed:set, -Children: pairs) is det.
% find all the children of the node *that aren't in the Closed list*,
% add the path travelled so far to them.
extended_list_children(GC, Node, Travelled, Closed, NewClosed, Children) :-
    get_children(GC, Node, ChildNodes),
    ord_union(Closed, ChildNodes, NewClosed, OpenChildNodes),
    zip_path([Node|Travelled], OpenChildNodes, Children).
extended_list_children(DL, DL, _, _, _, Closed, Closed, []).
extended_list_children(CD, DL, GC, Node, Travelled, Closed, NewClosed, Children) :-
    CD < DL,
    extended_list_children(GC, Node, Travelled, Closed, NewClosed, Cs),
    succ(CD, ND),
    zip_depth(ND, Cs, Children).

%! zip_depth(+Depth:int, +Nodes:list, -Pairs:pairs) is det.
% Add Depth as the second item in the pair Node-Path for each
% Node in Nodes.
zip_depth(D, Nodes, Pairs) :-
    zip_depth(D, Nodes, H-H, Pairs), !.
zip_depth(_, [], Acc-[], Acc).
zip_depth(D, [N|T], Acc-[N-D|NH], Pairs) :-
    zip_depth(D, T, Acc-NH, Pairs).

%! zip_path(+Path:list, +Nodes:list, -Out:compound) is det.
% Add Path as the second arg of node(Node, Path) for each
% Node in Nodes.
zip_path(Path, Nodes, Pairs) :-
    zip_path(Path, Nodes, H-H, Pairs), !.
zip_path(_, [], Acc-[], Acc).
zip_path(Path, [N|T], Acc-[node(N,Path)|NH], Pairs) :-
    zip_path(Path, T, Acc-NH, Pairs).

% zip_path based on values of pairs
zip_path_values(Path, Nodes, Out) :-
    pairs_keys_values(Nodes, Keys, Values),
    zip_path(Path, Values, Zipped),
    pairs_keys_values(Out, Keys, Zipped).

%! get_children(Node, Children) is det.
% Proxy to user defined search:get_children/2.
% User defined version may fail instead of return
% an empty list, this mitigates that.
get_children(GC, N, Children) :-
    ( call(GC, N, Children)
    ; \+ call(GC, N, _),
      Children = []
    ).

%! path_check(?Path, +Options)
% If path(Path) is in options, unify Path
path_check(Path, Options) :-
    memberchk(path(_), Options), member(path(Path), Options).
path_check(_, Options) :-
    \+ memberchk(path(_), Options).

add_cost(Cost, N-P, M-P) :-
    M is Cost + N.

%!  ord_union_on_values(+Set1, +Set2, -Union, -New) is det.
%
%   True iff ord_union(Set1, Set2, Union) and
%   ord_subtract(Set2, Set1, New) where the values of the pairs are the set.
ord_union_on_values([], Set2, Set2, Set2).
ord_union_on_values([H|T], Set2, Union, New) :-
    ord_union_1(Set2, H, T, Union, New).

ord_union_1([], H, T, [H|T], []).
ord_union_1([H2|T2], H, T, Union, New) :-
    compare_values(Order, H, H2),
    ord_union(Order, H, T, H2, T2, Union, New).

ord_union(<, H, T, H2, T2, [H|Union], New) :-
    ord_union_2(T, H2, T2, Union, New).
ord_union(>, H, T, H2, T2, [H2|Union], [H2|New]) :-
    ord_union_1(T2, H, T, Union, New).
ord_union(=, H, T, _, T2, [H|Union], New) :-
    ord_union(T, T2, Union, New).

ord_union_2([], H2, T2, [H2|T2], [H2|T2]).
ord_union_2([H|T], H2, T2, Union, New) :-
    compare_values(Order, H, H2),
    ord_union(Order, H, T, H2, T2, Union, New).

compare_values(Order, _-V1, _-V2) :-
    compare(Order, V1, V2).
