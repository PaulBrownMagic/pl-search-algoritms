:- module(queue,
    [ queue/1
    , queue/2
    , queue_head/3
    , queue_head_list/3
    , queue_last/3
    , queue_last_list/3
    , list_queue/2
    , queue_length/2
    ]
).

/** <module> Queue

An implementation of the queue data structure.

---

This material was reproduced with permission from "The Craft of Prolog" by Richard O'Keefe,
published by The MIT Press.

---

@see "The Art of Prolog" by Richard O'Keefe, pages 50-51
*/

%! queue(Queue).
% is true when Queue is a queue with no elements
queue(q(0, Q, Q)).

%! queue(X, Queue).
% is true when Queue is a queue with one element
queue(X, q(s(0), [X|Q], Q)).

%! queue_head(X, Q1, Q0).
% is true when Q0 and Q1 have the same elements except
% that Q0 has in addition X at the front.
% Can be used for enqueuing and dequeuing both.
queue_head(X, q(N, F, B), q(s(N), [X|F], B)).

%! queue_head_list(List, Q1, Q0).
% is true when append(List, Q1, Q0) would be true
% if Q1 & Q2 were lists instead of queues.
% Can be used for batch operations.
queue_head_list([], Q, Q).
queue_head_list([X|Xs], Q, Q0) :-
    queue_head(X, Q1, Q0),
    queue_head_list(Xs, Q, Q1).

%! queue_last(+X, Q1, Q0).
% is true when Q0 and Q1 have the same elements except
% that Q0 has in addition X at the end
queue_last(X, q(N, F, [X|B]), q(s(N), F, B)).

%! queue_last_list(List, Q1, Q0).
% is true when append(Q1, List, Q0) would be true
% if Q1 & Q2 were lists instead of queues.
% Can be used for batch operations
queue_last_list([], Q, Q).
queue_last_list([X|Xs], Q1, Q) :-
    queue_last(X, Q1, Q2),
    queue_last_list(Xs, Q2, Q).

%! list_queue(List, Queue).
% is true when List is a list and Queue is a queue and
% they represent the same sequence
list_queue(List, q(Count, Front, Back)) :-
    list_queue(List, Count, Front, Back).

list_queue([], 0, B, B).
list_queue([X|Xs], s(N), [X|F], B) :-
    list_queue(Xs, N, F, B).

%! queue_length(+Queue, -Length).
% is true when Length is (a binary integer representing)
% the number of elements in (the queue represented by) Queue.
queue_length(q(Count, Front, Back), Length) :-
    queue_length(Count, Front, Back, 0, Length).

queue_length(0, Back, Back, Length, Length).
queue_length(s(N), [_|Front], Back, L0, Length) :-
    succ(L0, L1),
    queue_length(N, Front, Back, L1, Length).
