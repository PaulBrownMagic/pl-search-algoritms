:- begin_tests(bfs).

:- use_module('../prolog/search/bfs').

edge(a, b).
edge(b, c).
edge(b, d).
edge(c, d).
edge(d, e).

get_children(Node, Children) :-
    setof(Child, edge(Node, Child), Children).

my_goal(N) :-
    N = e.

test(origin_included,
    [ nondet
    , true(A = a)
    ]
) :-
    bfs(get_children, a, A).

test(one_edge,
    [ nondet
    , true(B = b)
    ]
) :-
    dif(a, B), % skip over origin case
    bfs(get_children, a, B).

test(reachable_a, [nondet]) :- bfs(get_children, a, a).
test(reachable_b, [nondet]) :- bfs(get_children, a, b).
test(reachable_c, [nondet]) :- bfs(get_children, a, c).
test(reachable_d, [nondet]) :- bfs(get_children, a, d).
test(reachable_e, [nondet]) :- bfs(get_children, a, e).

test(extended_list,
    [ true(Ns = [a, b, c, d, e]) % In order
    ]
) :-
    findall(X, bfs(get_children, a, X), Ns).

test(no_extended_list,
    [ true(Ns = [a, b, c, d, d, e, e]) % In order w/dupes
    ]
) :-
    findall(X, bfs(get_children, a, X, [extended_list(false)]), Ns).

test(custom_goal,
    [ true(X = e)
    , nondet
    ]
) :-
    bfs(get_children, my_goal, a, X, []).

test(goal_path,
    [ true(X-Path = e-[a, b, d, e])
    , nondet
    ]
) :-
    bfs(get_children, my_goal, a, X, [path(Path)]).

test(all_goal_paths,
    [ true(Paths = [e-[a, b, d, e], e-[a, b, c, d, e]])
    , nondet
    ]
) :-
    findall(X-Path, bfs(get_children, my_goal, a, X, [path(Path), extended_list(false)]), Paths).
:- end_tests(bfs).
