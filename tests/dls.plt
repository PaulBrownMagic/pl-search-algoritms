:- begin_tests(dls).

:- use_module('../prolog/search/dls').

edge(a, b).
edge(b, c).
edge(b, d).
edge(c, d).
edge(d, e).

get_children(Node, Children) :-
    setof(Child, edge(Node, Child), Children).

my_goal(N) :-
    N = e.

test(origin_included,
    [ nondet
    , true(A = a)
    ]
) :-
    dls(get_children, a, A, [limit(1)]).

test(fail_on_zero_limit,
    [ fail
    ]
) :-
    dls(get_children, a, _, [limit(0)]).

test(fail_on_no_limit,
    [ fail
    ]
) :-
    dls(get_children, a, _, []).

test(fail_on_var_limit,
    [ fail
    ]
) :-
    dls(get_children, a, _, [limit(_)]).

test(fail_on_neg_limit,
    [ fail
    ]
) :-
    dls(get_children, a, _, [limit(-1)]).

test(one_edge,
    [ nondet
    , true(B = b)
    ]
) :-
    dif(a, B), % skip over origin case
    dls(get_children, a, B, [limit(2)]).

test(reachable_a, [nondet]) :- dls(get_children, a, a, [limit(1)]).
test(reachable_b, [nondet]) :- dls(get_children, a, b, [limit(2)]).
test(reachable_c, [nondet]) :- dls(get_children, a, c, [limit(3)]).
test(reachable_d, [nondet]) :- dls(get_children, a, d, [limit(4)]).
test(reachable_e, [nondet]) :- dls(get_children, a, e, [limit(4)]).

test(extended_list,
    [ true(Ns = [a, b, c, d, e]) % In order
    ]
) :-
    findall(X, dls(get_children, a, X, [limit(5)]), Ns).

test(no_extended_list,
    [ true(Ns = [a, b, c, d, e, d, e]) % In order w/dupes
    ]
) :-
    findall(X, dls(get_children, a, X, [limit(5), extended_list(false)]), Ns).

test(custom_goal,
    [ true(X = e)
    , nondet
    ]
) :-
    dls(get_children, my_goal, a, X, [limit(5)]).

test(goal_path,
    [ true(X-Path = e-[a, b, d, e])
    , nondet
    ]
) :-
    dls(get_children, my_goal, a, X, [limit(4), path(Path)]).

test(all_goal_paths,
    [ true(Paths = [e-[a, b, c, d, e], e-[a, b, d, e]])
    , nondet
    ]
) :-
    findall(X-Path, dls(get_children, my_goal, a, X, [limit(5), path(Path), extended_list(false)]), Paths).

:- end_tests(dls).
