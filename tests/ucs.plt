:- begin_tests(ucs).

:- use_module('../prolog/search/ucs').

edge(a, b, 2).
edge(b, c, 1).
edge(b, d, 5).
edge(c, d, 1).
edge(d, e, 2).

get_children(Node, Children) :-
    setof(Cost-Child, edge(Node, Child, Cost), Children).
my_goal(N) :-
    N = e.

test(origin_included,
    [ nondet
    , true(A = a)
    ]
) :-
    ucs(get_children, a, A).

test(one_edge,
    [ nondet
    , true(B = b)
    ]
) :-
    dif(a, B), % skip over origin case
    ucs(get_children, a, B).

test(reachable_a, [nondet]) :- ucs(get_children, a, a).
test(reachable_b, [nondet]) :- ucs(get_children, a, b).
test(reachable_c, [nondet]) :- ucs(get_children, a, c).
test(reachable_d, [nondet]) :- ucs(get_children, a, d).
test(reachable_e, [nondet]) :- ucs(get_children, a, e).


test(all_solutions,
    [ true(Ns = [a, b, c, d, e, d, e]) % In order w/dupes
    ]
) :-
    findall(X, ucs(get_children, a, X), Ns).

test(all_costs,
    [ true(Ns = [a-0, b-2, c-3, d-4, e-6, d-7, e-9]) % In order w/dupes
    ]
) :-
    findall(X-C, ucs(get_children, a, X, [cost(C)]), Ns).

test(custom_goal,
    [ true(X = e)
    , nondet
    ]
) :-
    ucs(get_children, my_goal, a, X, []).

test(goal_path,
    [ true(X-Path = e-[a, b, c, d, e])
    , nondet
    ]
) :-
    ucs(get_children, my_goal, a, X, [path(Path)]).

test(all_goal_paths,
    [ true(Paths = [e-[a, b, c, d, e], e-[a, b, d, e]])
    , nondet
    ]
) :-
    findall(X-Path, ucs(get_children, my_goal, a, X, [path(Path)]), Paths).
:- end_tests(ucs).
