name(search_algorithms).
version('0.0.1').
title('General Search Algorithms (DFS, BFS, A* etc.)').
author('Paul Brown', 'paul@paulbrownmagic.com').
home('https://gitlab.com/PaulBrownMagic/search-algorithms').
download('https://gitlab.com/PaulBrownMagic/search-algorithms/*').
requires('rtp_qsndqs').
