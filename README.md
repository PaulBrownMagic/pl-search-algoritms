# Search Algorithms

This will be a pack for SWI-Prolog installation.

Imagined example usage:

```
get_children(N, Children) :-
    setof(Child, my_pred(N, Child), Children).

my_goal(N) :-
    N @< 3.

:- use_module(libary(search)).

search(get_children, my_goal, 1, X, [method(bfs), path(Path)]).


:- use_module(library(search/dfs)).

dfs(get_children, my_goal, 1, X, []).
```

Progress so far:
- DFS
- DLS
- BFS
- Iterative Deepening
- UCS

Algorithms to include:
- A*
